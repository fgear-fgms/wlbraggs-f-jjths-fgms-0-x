#ifndef __HTTPSOURCE_HXX
#define __HTTPSOURCE_HXX

#include <string>
#include <vector>
#include <curl/curl.h>

class HTTPSource {
public:
 HTTPSource();
~HTTPSource();
protected:
  virtual void initURLs() = 0;
  void fetchURLs();
private:
  CURL *m_curl_handle;
  std::vector<std::string> m_string_list;
};

#endif
