//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, U$
//
// Copyright (C) 2012  Clément de l'Hamaide
//

/* This part of FGMS is called FGAIS for Flightgear AI Server
 * FGAIS can be considered as an addon like FG_Tracker
 * 
 * The goal of FGAIS is simple : send live traffic to FG client as AI traffic
 * For more information about FGAIS : http://wiki.flightgear.org/FGAIS
 * 
 *
 *
 * HOW IT WORKS ?
 * FGAIS is splitted in 3 main parts : 
 *
 * 1) Download live traffic :
 *          This part is done in a separate thread
 *          in order to avoid freezing FGMS during the http request.
 *          We make a request every 60 seconds
 *
 * 2) Manage the downloaded live traffic who consist to :
 *          This part is done in the same separate thread than 1)
 *          a) Parse the downloaded live traffic (we receive a kind of JSON feed)
 *          b) Create a list of AI object (aircraft, ship...)
 *          c) Calculate the needed data for send it to FG client
 *
 * 3) Send the live traffic to FG client who consist to :
 *          a) Know the @IP of the current FG client
 *             this information come from fg_server.cxx:1480
 *          b) Select only AI object in area of the player
 *          c) Compute the position of the AI object for the current timestamp     ===============> Work in progress
 *             Since we receive the new positions every 60 seconds we need to compute
 *             the position of the AI between these 2 request
 *          d) Create an MP packet who contain all the positions
 *             for each AI object selected.
 *          e) Send the created MP packet to the client
 *
 * The 3) is done in the main FGMS thread and done each time that FGMS receive an MP packet.
 * It imply to freeze FGMS during this part of FGAIS.
 *
 */

#include <cstring>
#include <cstdlib>

#include <iostream>
#include <sstream>
#include <map>

#include "../flightgear/MultiPlayer/mpmessages.hxx"
#include "fg_geometry.hxx"
#include "fg_aimgr.hxx"

#define EXTRAPOLATION_INTERVAL 60 	// provide packets for 60 seconds
#define REQUEST_DELAY 60		// start downloading, parsing, computing every 60 seconds

static string curlReturn;

using namespace std;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// ctor & dtor will be only called by the fgms main thread
AI_Mgr::AI_Mgr // Constructor
()
{
  stringstream    urlConstruct;
  double         tileScale(90.00);
  m_curl_handle = curl_easy_init();  //only needs to be done in the constructor once!

  // URL building only needs to be done once in the constructor, URLs are cached in a vector and re-used later on
  for(double x=-180; x<180; x+=tileScale){
    for (double y=-90; y<90; y+=tileScale){
      urlConstruct.str("");
      urlConstruct << "http://planefinder.net/endpoints/update.php?faa=1&bounds=" << y << "%2C" << x << "%2C" << y+tileScale << "%2C" << x+tileScale;
      m_url_list.push_back( urlConstruct.str() );
    }
  }
} // of AI_Mgr constructor


AI_Mgr::~AI_Mgr // Destructor
()
{
  curl_easy_cleanup(m_curl_handle); // only needs to be done in the dtor!
} // of ~AI_Mgr 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AI_Mgr::run // runs in separate worker thread
()
{
  update();
} // of run()

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// CURL callbacks (run in the FGAIS worker thread, not the fgms main thread) :

size_t writeAircraftCallback_helper
( char* buf, size_t size, size_t nmemb, void* p )
{
  return static_cast<AI_Mgr*>(p)->writeAircraftCallback(buf, size, nmemb);
}


size_t AI_Mgr::writeAircraftCallback
( char* buf, size_t size, size_t nmemb )
{
  for (size_t c = 0; c<size*nmemb; c++){
    curlReturn.push_back(buf[c]);
  }
  return size*nmemb;
} 


//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void AI_Mgr::download // this routine will go through all URLs in the URL vector and update the m_AircraftTraffic vector so that it contains the JSON data
()
{
  CURLcode        res;
  stringstream    urlConstruct;

  std::cout << "Fetching new data from planefinder.net" << std::endl;

  SGTimeStamp start = SGTimeStamp::now();
  for (URL_List::iterator i=m_url_list.begin(); i != m_url_list.end(); i++) //TODO: clean up!
  {
    urlConstruct.str("");
    urlConstruct << (*i);
    // std::cout << "  URL:" << urlConstruct.str() << std::endl;
    curl_easy_setopt(m_curl_handle, CURLOPT_URL, urlConstruct.str().c_str() );
    curl_easy_setopt(m_curl_handle, CURLOPT_WRITEFUNCTION, &writeAircraftCallback_helper);
    res = curl_easy_perform(m_curl_handle); // Perform the request, res will get the return code
    if(res != CURLE_OK) // Check for errors 
      fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    m_AircraftTraffic.push_back(curlReturn);
    curlReturn = "";
  }
  SGTimeStamp end = SGTimeStamp::now();
  std::cout << "Downloading took:" << (end - start).toSecs() << std::endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AI_Mgr::parse
()
{
  string          s;
  int             nbTabl;
  vector<string>  VecStrX, VecStr;
  SGTimeStamp start = SGTimeStamp::now();
  for(size_t n=0; n<m_AircraftTraffic.size(); n++)
  {
    if(m_AircraftTraffic[n] != "{\"planes\":[],\"isPartial\":true}")
    {
      s = m_AircraftTraffic[n];
      nbTabl = split(VecStrX, s, "{");

      if(nbTabl > 3)
      {
        aircraftParser(VecStr, VecStrX[2]);
        aircraftParser(VecStrX, VecStrX[3]);
      }
      else
      {
        aircraftParser(VecStrX, VecStrX[2]);
      }
    }
  }
  SGTimeStamp end = SGTimeStamp::now();
  std::cout << "Parsing took:" << (end - start).toSecs() << " (" << double (m_AircraftList.size()) / 1000 << "k aircraft)" << std::endl;
  // std::cout << "Memory consumption by active AircraftList is:" << m_AircraftList.size() * sizeof(AI_Aircraft)/1024 << " kb" << std::endl;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
 * Function FillMsgHdr
 *
 * Fill header of network packets
 */

void AI_Mgr::FillMsgHdr
( T_MsgHdr *MsgHdr, int MsgId, unsigned _len, string callsign )
{
  uint32_t len;
  switch (MsgId) {
  case CHAT_MSG_ID:
    len = sizeof(T_MsgHdr) + sizeof(T_ChatMsg);
    break;
  case POS_DATA_ID:
    len = _len;
    break;
  default:
    len = sizeof(T_MsgHdr);
    break;
  }
  MsgHdr->Magic           = XDR_encode<uint32_t>(MSG_MAGIC);
  MsgHdr->Version         = XDR_encode<uint32_t>(PROTO_VER);
  MsgHdr->MsgId           = XDR_encode<uint32_t>(MsgId);
  MsgHdr->MsgLen          = XDR_encode<uint32_t>(len);
  MsgHdr->ReplyAddress    = 0; // Are obsolete, keep them for the server for
  MsgHdr->ReplyPort       = 0; // now
  strncpy(MsgHdr->Callsign, callsign.c_str(), MAX_CALLSIGN_LEN);
  MsgHdr->Callsign[MAX_CALLSIGN_LEN - 1] = '\0';
} // of FillMsgHdr

void AI_Mgr::recompute_aircraft(boost::shared_ptr<AI_Item> ac)
{
      ////////////////////////////////////////////////////////
      //////////////// Extrapolator routine //////////////////
      ////////////////////////////////////////////////////////
        FGExternalMotionData    motionInfo;
        SGVec3d                 ecPos;
        SGQuatf                 ecOrient;
        Point3D                 AIPosGeod;
        SGVec3d                 AIPosCart;

        AIPosCart[X] = ac->LastPos.GetX();
        AIPosCart[Y] = ac->LastPos.GetY();
        AIPosCart[Z] = ac->LastPos.GetZ();

        // position
        motionInfo.position = AIPosCart;
        // orientation
        sgCartToGeod(ac->LastPos, AIPosGeod);
        SGQuatf qEc2Hl = SGQuatf::fromLonLatDeg((float)AIPosGeod[Lon], (float)AIPosGeod[Lat]);
        SGQuatf hlOr = SGQuatf::fromYawPitchRollDeg(ac->LastOrientation[X],  // hdg
                                                    ac->LastOrientation[Y],  // pitch
                                                    ac->LastOrientation[Z]); // roll
        motionInfo.orientation = qEc2Hl*hlOr;
        // velocity
        motionInfo.linearVel = SGVec3f(ac->Airspeed/1.68780986,
                                                        0.0,
                                                        SG_FEET_TO_METER*(ac->Climbrate/60));
        motionInfo.angularVel = SGVec3f(0.0, 0.0, 0.0);
        // acceleration
        motionInfo.linearAccel = SGVec3f::zeros();
        motionInfo.angularAccel = SGVec3f::zeros();

        // Ok, we need to predict the future, so, take the best data we can have
        // and do some eom computation to guess that for now.


        // Do a few explicit euler steps with the constant acceleration's
        // This must be sufficient ...
        ecPos = motionInfo.position;
        ecOrient = motionInfo.orientation;
        SGVec3f linearVel = motionInfo.linearVel;
        SGVec3f angularVel = motionInfo.angularVel;

        for (TIMESPAN timeOffset=0; timeOffset < EXTRAPOLATION_INTERVAL; timeOffset++) {

          SGVec3d ecVel = toVec3d(ecOrient.backTransform(linearVel));
          ecPos += ecVel;
          ecOrient += ecOrient.derivative(angularVel);

          linearVel += (cross(linearVel, angularVel) + motionInfo.linearAccel);
          angularVel += motionInfo.angularAccel;

          ////////////////////////////////////////////////////////
          ////////////// MP packet creator routine ///////////////
          ////////////////////////////////////////////////////////

          // The current simulation time we need to update for,
          // note that the simulation time is updated before calling all the
          // update methods. Thus it contains the time intervals *end* time.
          // The FDM is already run, so the states belong to that time.
          motionInfo.time         = m_LastAIUpdate.toSecs()+timeOffset;
          motionInfo.lag          = 0.1;
          motionInfo.position     = ecPos;
          motionInfo.orientation  = ecOrient;

          boost::shared_ptr<MsgBuf> msgBuf(new MsgBuf);
          static unsigned msgLen = 0;
          T_PositionMsg* PosMsg = msgBuf->posMsg();
          xdr_data_t* ptr = msgBuf->properties();

          strncpy(PosMsg->Model, ac->ModelName.c_str(), MAX_MODEL_NAME_LEN);
          PosMsg->Model[MAX_MODEL_NAME_LEN - 1] = '\0';

          PosMsg->time = XDR_encode64<double> (motionInfo.time);
          PosMsg->lag = XDR_encode64<double> (motionInfo.lag);
          for (unsigned i = 0 ; i < 3; ++i)
            PosMsg->position[i] = XDR_encode64<double> (motionInfo.position(i));
          SGVec3f angleAxis;
          motionInfo.orientation.getAngleAxis(angleAxis);
          for (unsigned i = 0 ; i < 3; ++i)
            PosMsg->orientation[i] = XDR_encode<float> (angleAxis(i));
          for (unsigned i = 0 ; i < 3; ++i)
            PosMsg->linearVel[i] = XDR_encode<float> (motionInfo.linearVel(i));
          for (unsigned i = 0 ; i < 3; ++i)
            PosMsg->angularVel[i] = XDR_encode<float> (motionInfo.angularVel(i));
          for (unsigned i = 0 ; i < 3; ++i)
            PosMsg->linearAccel[i] = XDR_encode<float> (motionInfo.linearAccel(i));
          for (unsigned i = 0 ; i < 3; ++i)
            PosMsg->angularAccel[i] = XDR_encode<float> (motionInfo.angularAccel(i));

          msgLen = reinterpret_cast<char*>(ptr) - msgBuf->Msg;
	  std::string callsign_workaround = (ac->Callsign == "z.NO-REG")? ac->Hexcode : ac->Callsign; // if no callsign, use the hexcode as callsign
          FillMsgHdr(msgBuf->msgHdr(), POS_DATA_ID, msgLen, ac->Callsign); 

// Validate the created message
if (msgLen < sizeof(T_MsgHdr) + sizeof(T_PositionMsg))
    {
      std::string ErrorMsg;
      ErrorMsg += "Wrong POS_DATA_ID packet created with insufficient position data, ";
#if 0
      ErrorMsg += "should be ";
      ErrorMsg += NumToStr (sizeof(T_MsgHdr)+sizeof(T_PositionMsg));
      ErrorMsg += " is: " + NumToStr (msgBuf.MsgHdr->MsgLen);
#endif

      std::cout << ErrorMsg << std::endl;
      exit(-1);
}


#if 0
// Some more packet validation
{
	T_PositionMsg* PosMsg = (T_PositionMsg *) (msgBuf.Msg + sizeof(T_MsgHdr));
	double x = XDR_decode64<double> (PosMsg->position[X]);
	double y = XDR_decode64<double> (PosMsg->position[Y]);
	double z = XDR_decode64<double> (PosMsg->position[Z]);
	std::cout <<"X: " << ac->LastPos.GetX() << " Packet:" << x << std::endl;
        std::cout <<"Y: " << ac->LastPos.GetY() << " Packet:" << y << std::endl;
        std::cout <<"Z: " << ac->LastPos.GetZ() << " Packet:" << z << std::endl;
}
#endif
          ac->_timespan_positions[timeOffset].first = msgBuf;
          ac->_timespan_positions[timeOffset].second = msgLen;
	  //std::cout << "Finished recompute_aircraft() for callsign: " << ac->Callsign << "(Hexcode:" << ac->Hexcode <<")" << std::endl;

        } // of timespan loop
  

} // of recompute_aircaft method

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//FIXME: This gets currently invoked after 60 seconds have passed, which is just wrong, because by then,
// the PositionMap is already empty, so that the main thread needs to wait for the FGAIS thread to finish
// in other words, this should be done much earlier, or even interleaved
// See: pthreads barriers, condition variables and pthread joins
// FIXME: Getting a segfault here when using more than one fgfs client ...
void AI_Mgr::recompute //TODO: support an SGTimeStamp-based timeout, so that the function can return early
() 
{
  /* Description: populates an aircraft-specific std::map<TIMESPAN, MsgBuf> that contains extrapolated positions for the whole 60-second time span
   *
   * Here we will extrapolate the position for every AI in the world currently
   * We will store 60 MP packet containing 60 positions (1 positions per seconds per packets) in a std::map<float, MsgBuf>
   * where float = seconds offset, and MsgBuf = MP packet of the AI at this offset
   * For example :  std::map<23, MsgBuf>  means that we will send the MP packet
   * of the AI when 23 seconds are passed since our last AI traffic download
   *
   * At the end of this function, every AI contains 60 MP packet containing 60 positions (1 positions per seconds per packets)
   * in their  PositionMap  variable. 
   * We should access it like : myAI->PositionMap[secondsOffset]->second
   *
   */

  // std::cout << "recompute() will compute "<< EXTRAPOLATION_INTERVAL <<" positions for " << m_AircraftList.size() << " aircraft" << std::endl;
  std::cout << "Running now recompute()" << std::endl;
  SGTimeStamp start = SGTimeStamp::now();
  /* 
	FIXME: don't traverse the whole list, but just process requested positions based on list of "CurrentPlayer"

       go through list of active players
       foreach active player determine list of aircraft within range of 100 nm
       -> check if required timespan packets are available already or not
       if not: foreach aircraft within range of active player, compute packets for 60 seconds (should be cached)
       set pointer to AI_Item class in FG_Player class
        - afterwards continue with aircraft likely to get into range "soon"
        - then with remaining aircraft
   */

  // go through a list of local players and update all traffic within range, and precompute the corresponding MP messages
  for (PlayerTable::iterator i = _players.begin() ; i != _players.end() ; ++i)
  {
	std::cout << "Updating aircraft within range for client:" << i->second->Callsign <<std::endl;
	getAITrafficWithinRange( i->second ); // store pointers to relevant AI traffic in the FG_Player struct
	// Next, create timespan packets for each relevant aircraft that is in range:
	// std::cout << "Now, populating the PositionMap for each aircraft within 100 nm (#:" << i->second->relevantAirborneTraffic.size() << ")" <<std::endl; 
	for (	std::vector<boost::shared_ptr<AI_Item> >::iterator n = i->second->relevantAirborneTraffic.begin(); 
		n != i->second->relevantAirborneTraffic.end(); 
		n++) 
  			recompute_aircraft( *n ); // populate timespan map with MP messages for each time offset (0..59 for now)
  } // of relevant aircraft loop (in range)
  SGTimeStamp end = SGTimeStamp::now();
  std::cout <<"recompute() took:" << (end - start).toSecs() << std::endl;

} // of recompute

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AI_Mgr::update
()
{
  // 1- download aircraft traffic from planefinder.net
  // 2- parse aircraft traffic and create an Aircraft object for each item parsed

  SGTimeStamp start, end;

  while(1)
  {

    if ( m_AircraftList.size() > 0 )
    {
      m_deadlist.push_back ( m_PrevAircraftList );  // save the old list to prevent the smart pointers from kicking in here (and do it at the end)
      m_PrevAircraftList = m_AircraftList; 
    }

    m_AircraftList.clear(); // clear the parsed list of aircraft
    m_AircraftTraffic.clear(); // clear the JSON buffer

    start = SGTimeStamp::now();

    download(); // get the latest data from the provider
    m_LastAIUpdate = SGTimeStamp::now();
    lock();	// now lock the internal data structures
      parse();  // parse the downloaded data and update the data structures
      recompute(); // recompute positions/orientation etc
    unlock();   // unlock the data structures
    //cout << "Nb of AI aircraft : " << m_AircraftList.size() << endl;

    //FIXME: use a real task scheduler here (such as SGEventMgr)
    //SGTimeStamp free_dead = SGTimeStamp::now();
    m_deadlist.clear();
    // std::cout << "Freeing dead: " << (SGTimeStamp::now()-free_dead).toSecs() << std::endl;
    end = SGTimeStamp::now();

    SGTimeStamp delta = end -start;
    if( delta.toSecs() < (REQUEST_DELAY/2))
    {
      sleep( REQUEST_DELAY - (delta.toSecs()*2) );
      //TODO: we can run cleanup code here
      
    }
  }

}

void AI_Mgr::getAITrafficWithinRange( FG_Player* player, const size_t range_nm ) {
player->relevantAirborneTraffic.clear(); //FIXME: doing this is pretty dumb and inefficient 
// FIXME: we should really be using a spatial data structure (quadtree/geohash) here!
for (m_AircraftListIt = m_AircraftList.begin() ; m_AircraftListIt != m_AircraftList.end() ; ++m_AircraftListIt)
    {
      if (Distance (player->LastPos, m_AircraftListIt->second->LastPos) <= range_nm) {
	// std::cout << "Found traffic within range:" << m_AircraftListIt->second->Callsign << std::endl;
	player->relevantAirborneTraffic.push_back( m_AircraftListIt->second );
	}
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// This function will be called by the fgms main thread !
// it should only READ the data structures and never WRITE to them (which is exclusively done by the worker thread for now)
void AI_Mgr::sendAITraffic
( mT_PlayerListIt& CurrentPlayer, netSocket* m_DataSocket )
{

  vector<AI_Item*> AircraftSelected;
  Point3D          AIPosGeod;


  // 1- get the list of local client ===>  OBSOLETE 
  // 2- get the list of Aircraft object => OK
  // 3- get the list of Ship object
  // 4- select Aircraft/Ship object in the area of the local client => OK for aircraft
  // 5- compute/extrapolate the position of Aircraft/Ship
  // 6- create the network packet who contain the new Aircraft/Ship position
  // 7- send the network packet to the client

  SGTimeStamp lock_start = SGTimeStamp::now(); // the time when we try to get the lock
  lock();
  if ((CurrentPlayer->IsLocal) && (m_AircraftList.size() > 0))
  {
    PlayerTable::iterator thisPlayer = _players.find(CurrentPlayer->Callsign); // try to find the current player in player table

    if (thisPlayer == _players.end() ) {
	// std::cout << "Player not yet known, adding new entry (not sending any MP messages for one minute):" << CurrentPlayer->Callsign << std::endl;
	_players[CurrentPlayer->Callsign] = &*CurrentPlayer; // player not found, so add new entry (using callsign as key for now)
	//TODO: handle expired players/clients
	// HACK:
	getAITrafficWithinRange( &*CurrentPlayer );
	for (   std::vector<boost::shared_ptr<AI_Item> >::iterator n = CurrentPlayer->relevantAirborneTraffic.begin();
                n != CurrentPlayer->relevantAirborneTraffic.end();
                n++)
                        recompute_aircraft( *n ); // populate timespan map
	//TODO: send packet

	}
//    else {

	// std::cout << "Ok, player is known:" << CurrentPlayer->Callsign << std::endl;
	// TODO: check if timespan table is available?
	//std::cout << "Total aircraft in range:" << CurrentPlayer->relevantAirborneTraffic.size() <<std::endl;

	for (std::vector<boost::shared_ptr<AI_Item> >::iterator i = CurrentPlayer->relevantAirborneTraffic.begin();
                i != CurrentPlayer->relevantAirborneTraffic.end();
                i++)
        {
	AI_ItemPtr ac (*i); //FIXME: huge hack, change the method's signature!
	//if (ac->Callsign == "z.NO-REG") continue;
	// std::cout << "Delta_T is:" << time(0) - m_LastAIUpdate;
	unsigned char offset = (unsigned char) ((SGTimeStamp::now() -m_LastAIUpdate).toSecs() ); // compute the required offset into the lookup table

#if 0
// Some offset and positionMap checking:
	if (offset > EXTRAPOLATION_INTERVAL) throw("requested offset beyond configured extrapolation range");
	if (ac->_timespan_positions.find(offset) == ac->_timespan_positions.end() ) throw("Message offset not found in map!");
	if (ac->_timespan_len.find(offset) == ac->_timespan_len.end() ) throw("Length offset not found in PositionMap!");

	//std::cout << "Sending info for [" << (int) offset <<"]:hexcode: "<< ac->Hexcode << std::endl;
#endif

#if 0
// even more position map checking for debugging purposes:
    T_PositionMsg* PosMsg              = (T_PositionMsg *) (ac->_timespan_positions[offset].first->Msg + sizeof(T_MsgHdr));
    T_MsgHdr* MsgHdr                   = (T_MsgHdr *) ac->_timespan_positions[offset].first->Msg;
    double x = XDR_decode<float> (PosMsg->linearVel[X]);
    double y = XDR_decode<float> (PosMsg->linearVel[Y]);
    double z = XDR_decode<float> (PosMsg->linearVel[Z]);

    std::cout << "Sending an MP packet of " << MsgHdr->Callsign << " motionInfo.linearVel[x, y, z]=" << x << ", " << y << ", " << z << "]" << endl;
#endif	




	if (m_DataSocket->sendto (	ac->_timespan_positions[offset].first->Msg, 
				ac->_timespan_positions[offset].second, 
				0, 
				&CurrentPlayer->Address) == -1)
	std::cout << "Error sending the packet!" << std::endl;
	} // of loop sending relevant traffic to current player
//} // player found and known
   
  } // if a local player and aircraft list is populated

  unlock();
  SGTimeStamp delta = (SGTimeStamp::now()-lock_start);
  if (delta.toSecs() > 1)
	  std::cout << "fgms main thread was blocked by fgais for " << delta.toSecs() << " secs" << std::endl;

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AI_Mgr::addAIAircraft
( double lat, double lon, double alt, double hdg,
  double airspeed, string hexcode, string callsign, string model )
{

  AI_ItemPtr  NewAI(new AI_Item);
  // Add the created object to the list of Aircraft object
  m_AircraftList[hexcode] = NewAI;

  double       AICart[3];

  // 1- Create an Aircraft object //TODO: store planefindert.net timestamp and use it to avoid "rewinding"
  NewAI->Callsign  = callsign;
  NewAI->Hexcode   = hexcode;
  NewAI->ModelName = model;
  NewAI->Airspeed  = airspeed;
  NewAI->Timestamp = time(0);

  NewAI->LastPos.clear();
  NewAI->LastOrientation.clear();

  sgGeodToCart(lat*SGD_DEGREES_TO_RADIANS, lon*SGD_DEGREES_TO_RADIANS, alt*SG_FEET_TO_METER, AICart);
  NewAI->LastPos.Set (AICart[0], AICart[1], AICart[2]);
  NewAI->LastOrientation.Set (hdg, 0, 0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AI_Mgr::updateAIAircraft
( double lat, double lon, double alt, double hdg,
  double airspeed, string hexcode, AI_ItemPtr oldAI )
{

  AI_ItemPtr   NewAI = oldAI; // don't allocate a new object, re-use the old one
 // 2- Add the old object to the list of Aircraft object in the current map
  m_AircraftList[hexcode] = NewAI;
  double       AICart[3];
  Point3D       oldAIGeod;

  NewAI->Timestamp = time(0);
  sgCartToGeod (oldAI->LastPos, oldAIGeod);
  NewAI->Climbrate = (floor(oldAIGeod[Alt] - alt) != -1 ) ? floor(oldAIGeod[Alt] - alt) : 0;
  if ( NewAI->Climbrate != 0 ) NewAI->Climbrate++;

  NewAI->LastPos.clear();
  NewAI->LastOrientation.clear();

  sgGeodToCart(lat*SGD_DEGREES_TO_RADIANS, lon*SGD_DEGREES_TO_RADIANS, alt*SG_FEET_TO_METER, AICart);
  NewAI->LastPos.Set (AICart[0], AICart[1], AICart[2]);
  NewAI->LastOrientation.Set (hdg, 0, 0);  // Heading, Pitch, Roll

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AI_Mgr::updateOrAddAircraft
( double lat, double lon, double alt, double heading,
  double airspeed, string &hexcode, string callsign, string model )
{
  if ( m_PrevAircraftList.size() > 0 )
  {
    m_PrevAircraftListIt = m_PrevAircraftList.find(hexcode);
    if ( m_PrevAircraftListIt != m_PrevAircraftList.end() ) // Searching in the old list if this AI is present
    {
      updateAIAircraft( lat, lon, alt, heading, airspeed, hexcode, m_PrevAircraftListIt->second );
      return;
    }
 }
addAIAircraft(lat, lon, alt, heading, airspeed, hexcode, callsign, model);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 /* Function: aircraftParser()
  * Description: parse the received live traffic
  *              and create aircraft object
  */

void AI_Mgr::aircraftParser
( vector<string> VecStr, string s )
{
  int nbTabl;
  vector<string> VecStrA, VecStrB;
  VecStrA.clear();
  VecStrB.clear();

  if(s != "\"1\":")
  {
    nbTabl = split(VecStr, s, "}");
    s = VecStr[0];
    nbTabl = split(VecStr, s, "],");

    for(int i = 0; i < nbTabl; ++i)
    {
      split(VecStrA, VecStr[i], ":[");
      split(VecStrB, VecStrA[0], "\"");
      string hexcode = VecStrB[1];
      split(VecStrB, VecStrA[1], "[");
      split(VecStrA, VecStrB[1], "]");
      split(VecStrB, VecStrA[0], ",");
      split(VecStrA, VecStrB[0], "\"");
      string model = aircraftToPath(VecStrA[1]);
      split(VecStrA, VecStrB[1], "\"");
      string callsign  = VecStrA[1];
      double lat  = atof(VecStrB[3].c_str());
      double lon = atof(VecStrB[4].c_str());
      double alt  = atof(VecStrB[5].c_str());
      double heading   = atof(VecStrB[6].c_str());
      double airspeed  = atoi(VecStrB[7].c_str());

       updateOrAddAircraft(lat, lon, alt, heading, airspeed, hexcode, callsign, model);
    } // for
  }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* Function: split()
 * Description: split a string into vector
 *              with specified delimiter
 * Input: target vector which contain splitted string
 *        string to parse
 *        delimiter for parsing the string
 * Output: size of the vector who contain splitted string
 */

int AI_Mgr::split
( vector<string>& vecteur, string chaine, string delimiter )
{

  vecteur.clear();
  string::size_type stTemp = chaine.find(delimiter);
  while(stTemp != string::npos)
  {
    vecteur.push_back(chaine.substr(0, stTemp));
    chaine = chaine.substr(stTemp + 1);
    stTemp = chaine.find(delimiter);
  }
  vecteur.push_back(chaine);
  return vecteur.size();

} // split


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/* Function: aircraftToPath()
 * Description: convert and aircraft reference
 *              to AI aircraft path for Flightgear
 * Input: Aircraft reference like "B77L", "A343" ...
 * Output: AI aircraft path for Flightgear
 */

string AI_Mgr::aircraftToPath
( string aircraft )
{

  string aircraftPath;

  // Boeing :
  if (!strcmp(aircraft.c_str(), "B772"))
  {
    aircraftPath = "AI/Aircraft/777/777-200ER-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B773"))
  {
    aircraftPath = "AI/Aircraft/777/777-300-AirFrance.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B77W"))
  {
    aircraftPath = "AI/Aircraft/777/777-300-AirCanada.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B77L"))
  {
    aircraftPath = "AI/Aircraft/777/777-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B767"))
  {
    aircraftPath = "AI/Aircraft/767/767-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B763"))
  {
    aircraftPath = "AI/Aircraft/767/767-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B752"))
  {
    aircraftPath = "AI/Aircraft/752/752-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B744"))
  {
    aircraftPath = "AI/Aircraft/747-400/747-400-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B738"))
  {
    aircraftPath = "AI/Aircraft/737/737-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B737"))
  {
    aircraftPath = "AI/Aircraft/737/737-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "B734"))
  {
    aircraftPath = "AI/Aircraft/737/737-set.xml";
  }

  // Airbus :
  else if (!strcmp(aircraft.c_str(), "A343"))
  {
    aircraftPath = "AI/Aircraft/A343/A343-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "A333"))
  {
    aircraftPath = "AI/Aircraft/A333/A333-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "A332"))
  {
    aircraftPath = "AI/Aircraft/A332/A332-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "A319"))
  {
    aircraftPath = "AI/Aircraft/A321/A319-main.xml";
  }
  else if (!strcmp(aircraft.c_str(), "A320"))
  {
    aircraftPath = "AI/Aircraft/A320/A320-fb-set.xml";
  }
  else if (!strcmp(aircraft.c_str(), "A321"))
  {
    aircraftPath = "AI/Aircraft/A321/A321-main.xml";
  }

  // Default :
  else
  {
    aircraftPath = "AI/Aircraft/737/737-set.xml"; //FIXME: we should support some default models based on groundspeed (performance)
  }

  /*if(aircraftPath == "*unknown*")
    cout << aircraft << " : " << aircraftPath << endl;*/ 

  return aircraftPath;
}


