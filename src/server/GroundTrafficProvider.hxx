#ifndef __GROUNDRAFFICPROVIDER_HXX
#define __GROUNDTRAFFICPROVIDER_HXX

/*
 * This file is is intended for all providers that provide ground traffic.
 * He provide a definition of all common properties for every ground traffic.
 */

#include "TrafficProvider.hxx"

class GroundTrafficProvider : TrafficProvider {

};

#endif
