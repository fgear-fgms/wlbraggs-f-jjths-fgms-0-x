#ifndef __PLANEFINDERPROVIDER_HXX
#define __PLANEFINDERPROVIDER_HXX

/*
 * This file is dedicated to planefinder.com provider
 * Here we will define dedicated download(), parse() and update() methods for him
 */

#include "AirborneTrafficProvider.hxx"

class PlanefinderProvider : public AirborneTrafficProvider {

};

#endif
