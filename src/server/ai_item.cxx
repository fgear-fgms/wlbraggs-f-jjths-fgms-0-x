//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, U$
//
// Copyright (C) 2012  Clément de l'Hamaide
//

#include <string>
#include "ai_item.hxx"


AI_Item::AI_Item()  // Constructor
{

    Callsign    = "";
    Hexcode     = "";
    ModelName   = "";
    Airspeed    =  0;
    Course      =  0;
    Timestamp   =  0;
    Climbrate   =  0;

}

AI_Item::AI_Item      // Constructor
( const AI_Item& P )
{
  this->assign (P);
}


AI_Item::~AI_Item() {
// std::cout << "Destructor called for Callsign:" << Callsign << std::endl;
//for (PositionMap::iterator i=_timespan_positions.begin(); i != _timespan_positions.end(); i++)	
//delete i->second;

} // Destructor


AI_Aircraft::AI_Aircraft() : AI_Item() {} // Constructor

AI_Ship::AI_Ship() : AI_Item() {}   // Constructor


void
AI_Item::assign
(
	const AI_Item& P
)
{

  Callsign        = P.Callsign.c_str();
  ModelName       = P.ModelName.c_str();
  Hexcode         = P.Hexcode;
  Timestamp       = P.Timestamp;
  Airspeed        = P.Airspeed;
  Course          = P.Course;
  LastPos         = P.LastPos;
  LastOrientation = P.LastOrientation;

}
