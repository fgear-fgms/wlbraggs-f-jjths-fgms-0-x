//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, U$
//
// Copyright (C) 2012  Clément de l'Hamaide
//


#ifndef DEF_AIMGR
#define DEF_AIMGR

#define FGAIS_RANGE 25 // limited for testing purposes

// System includes
#include <curl/curl.h>

// STL includes
#include <vector>
#include <map>

// Boost includes
#include <boost/shared_ptr.hpp>

// SimGear includes
#include <simgear/threads/SGThread.hxx>
#include <simgear/timing/timestamp.hxx>

// FGMS includes
#include "fg_player.hxx"
#include "ai_item.hxx"

using namespace std;

class AI_Mgr : public SGThread, SGMutex {
  public:
	   void run();  // required by SGThread, will be executed in the worker thread to call update()
  private:
	   typedef std::vector<std::string> URL_List;
	   typedef boost::shared_ptr<AI_Item> AI_ItemPtr;
           typedef map<string, AI_ItemPtr >   mT_AircraftList;
           typedef mT_AircraftList::iterator   mT_AircraftListIt;

	   typedef std::map<std::string, FG_Player*> PlayerTable;
	   PlayerTable _players;

	   URL_List m_url_list;
           mT_AircraftList   m_AircraftList;     // contains the list of Aircraft object
           mT_AircraftList   m_PrevAircraftList; // contains the previous list of Aircraft object
	   std::vector<mT_AircraftList> m_deadlist; 

           mT_AircraftListIt m_AircraftListIt;
           mT_AircraftListIt m_PrevAircraftListIt;

           vector<string>    m_AircraftTraffic;  // contains the Json stream
	   CURL* 	     m_curl_handle;	 // will be re-used between CURL requests
  public:
           AI_Mgr(); // will be called by the main thread during initialization
           ~AI_Mgr(); // ditto

           typedef std::vector<FG_Player>      mT_PlayerList;
           typedef mT_PlayerList::iterator     mT_PlayerListIt;

	   // update() will only be called in the worker thread !!
           void   update  ();
	   // sendAITraffic() will be called by the fgms main thread !!
           void   sendAITraffic      ( mT_PlayerListIt& CurrentPlayer, netSocket* );

  private: // None of these will be directly invoked by the main thread
	   void  download(); // download the data from planefinder.net
	   void  parse();    // actually parse the JSON data and populate the data structures
	   void  recompute(); // re/compute the positions/orientation for all known aircraft and a whole time span
	   void recompute_aircraft(boost::shared_ptr<AI_Item> ); // recompute position messages for just a single aircraft
	   void getAITrafficWithinRange( FG_Player*, const size_t range_nm=FGAIS_RANGE ); // gets traffic within a configurable range in the vicinity of the player

           void  FillMsgHdr ( T_MsgHdr *MsgHdr, int MsgId, unsigned _len, string callsign ); // used to prebuild MP packets

	   void updateOrAddAircraft ( double lat, double lon, double alt, double heading, double airspeed, std::string &hexcode,
                                std::string callsign="", std::string model="");
           void   addAIAircraft      ( double lat, double lon, double alt, 
                                      double hdg, double airspeed, string hexcode, 
                                      string callsign, string model );
           void   updateAIAircraft   ( double lat, double lon, double alt,
                                      double hdg, double airspeed, string hexcode, AI_ItemPtr oldAI );
           int    split              ( vector<string>& vecteur, string chaine, string delimiter );
           void   aircraftParser     ( vector<string> VecStr, string s );
           string aircraftToPath      ( string aircraft );

 public: // These are the curl callbacks (running in the worker thread, NOT in the main thread) :
           size_t writeAircraftCallback          ( char* buf, size_t size, size_t nmemb );
 private:
           //time_t      m_LastAIUpdate;
	   SGTimeStamp m_LastAIUpdate;
           //pthread_t   AIthread;


};

#endif

