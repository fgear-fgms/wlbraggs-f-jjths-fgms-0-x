//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, U$
//
// Copyright (C) 2012  Clément de l'Hamaide
//

#ifndef DEF_AIITEM
#define DEF_AIITEM

#include <string>
#include <utility>
#include <map>

#include <boost/shared_ptr.hpp>

#include <time.h>
#include "fg_geometry.hxx"
#include "../flightgear/MultiPlayer/mpmessages.hxx"

using namespace std;

typedef unsigned int TIMESPAN; // range: 0..255 (seconds)
typedef std::pair<boost::shared_ptr<MsgBuf>, unsigned> MessageRecord;
typedef std::map<TIMESPAN, MessageRecord > PositionMap;
//typedef std::map<TIMESPAN, unsigned> PositionLen;

class AI_Item {

  public:
            string          Callsign;
            string          Hexcode;
            string          ModelName;
            double         Airspeed;
            double         Course;
            double         Climbrate;
            double          Timestamp;
            Point3D         LastPos;
            Point3D         LastOrientation;
	    PositionMap     _timespan_positions; // extrapolated Point3D positions for the whole 60-second timespan, for now
            // PositionLen     _timespan_len;

            AI_Item();
            AI_Item ( const AI_Item& P);
            ~AI_Item();

private:
	void assign ( const AI_Item& P );

};


class AI_Aircraft : public AI_Item {

  public:
           AI_Aircraft();
           //AI_Aircraft( const AI_Item& P);
};


class AI_Ship : public AI_Item {

  public:
           AI_Ship();
           //AI_Ship( const AI_Item& P);
};

#endif
