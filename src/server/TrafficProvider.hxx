#ifndef __TRAFFICPROVIDER_HXX
#define __TRAFFICPROVIDER_HXX

/*
 * This file is a generic traffic provider that provides any form of traffic.
 * It is at the top of the pyramide of the fgais addon, if you want explore fgais
 * it's a good start to read this file. Each traffic provider implements the interface
 * of the SGThread class: http://simgear.sourceforge.net/doxygen/classSGThread.html
 * This makes it possible to run certain methods asynchronously 
 *
 *
 *									      -----------------------
 *                                                                           [ SGThread (base class) ]
 *                                                                            -----------------------
 *                                                                                     |
 *                                                                            ---------------------
 *                                                                           [ TrafficProvider.hxx ]
 *                                                                            ---------------------
 *                                                                                     |
 *                                _____________________________________________________|___________________________________________________
 *                               |                                                     |                                                   |
 *                               |                                                     |                                                   |
 *                  -----------------------------                         ---------------------------                         ---------------------------
 *                 [ AirborneTrafficProvider.hxx ]                       [ VesselTrafficProvider.hxx ]                       [ GroundTrafficProvider.hxx ]
 *                 [        (aircrafts)          ]                       [          (ships)          ]                       [         (ground)          ]
 *                  -----------------------------                         ---------------------------                         ---------------------------
 *                               |                                                     |                                                   |
 *               ________________|________________                                     |                                                   |
 *              |                                 |                                    |                                                   |
 *              |                                 |                                    |                                                   |
 *    ---------------------            -----------------------               ---------------------                               ---------------------
 *   [ PlaneFinderProvider ]          [ FlightRadar24Provider ]             [  ShipFinderProvider ]                             [       SUMOProvider ]
 *   [      .hxx/.cxx      ]          [       .hxx/.cxx       ]             [      .hxx/.cxx      ]                             [         .hxx/.cxx  ]
 *    ---------------------            -----------------------               ---------------------                               ---------------------
 *
 * Regarding "sumo", see: http://flightgear.org/forums/viewtopic.php?f=23&t=9553&hilit=sumo
 *
 *
 */

#include <simgear/threads/SGThread.hxx>


// to be implemented for each provider, i.e. the download() method
class TrafficSource {
public:
protected:
  virtual void update() = 0;
private:
};

// to be implemented for each provider, i.e. the parse() method
class TrafficParser {
public:
protected:
private:
};

// to be implemented for each provider, i.e. the recompute() stuff
class TrafficProcessor {
public:
protected:
private:
};

// to be implemented for FGMS or HLA
class TrafficInjector {
public:
protected:
private:
};

class FGMSInjector : public TrafficInjector {
public:
protected:
private:
};

class TrafficProvider : public SGThread {
public:
  TrafficProvider();
  ~TrafficProvider();
protected:
  virtual void download() = 0;	// calls the provider-specific traffic source, i.e. download()
  virtual void parse() = 0;	// calls the provider-specific parser, i.e. parse()
  virtual void process() = 0; 	// calls the provider-specific processor, i.e. recompute()
  virtual void inject() = 0;	// 
private:
  TrafficSource* 	m_source;
  TrafficParser* 	m_parser;
  TrafficProcessor* 	m_provider;
  TrafficInjector*	m_protocol;
};

#endif
