// mpmessages.hxx -- Message definitions for multiplayer communications
// within a multiplayer Flightgear
//
// Written by Duncan McCreanor, started February 2003.
// duncan.mccreanor@airservicesaustralia.com
//
// Copyright (C) 2003  Airservices Australia
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

#ifndef MPMESSAGES_H
#define MPMESSAGES_H

#define MPMESSAGES_HID "$Id: mpmessages.hxx,v 1.1.1.1 2007/06/12 10:10:24 oliver Exp $"

/****************************************************************
* @version $Id: mpmessages.hxx,v 1.1.1.1 2007/06/12 10:10:24 oliver Exp $
*
* Description: Each message used for multiplayer communications
* consists of a header and optionally a block of data. The combined
* header and data is sent as one IP packet.
*
******************************************************************/

#include <vector>
#include <cstring>
#include <simgear/compiler.h>
#include <simgear/math/SGMath.hxx>
#include "tiny_xdr.hxx"

// magic value for messages
const uint32_t MSG_MAGIC = 0x46474653;  // "FGFS"
// protocoll version
const uint32_t PROTO_VER = 0x00010001;  // 1.1

// Message identifiers
#define CHAT_MSG_ID             1
#define RESET_DATA_ID           6
#define POS_DATA_ID             7

// XDR demands 4 byte alignment, but some compilers use8 byte alignment
// so it's safe to let the overall size of a network message be a 
// multiple of 8!
#define MAX_CALLSIGN_LEN        8
#define MAX_CHAT_MSG_LEN        256
#define MAX_MODEL_NAME_LEN      96
#define MAX_PROPERTY_LEN        52
#define MAXI_PACKET_SIZE        1024

// Header for use with all messages sent 
struct T_MsgHdr {
    xdr_data_t  Magic;                  // Magic Value
    xdr_data_t  Version;                // Protocoll version
    xdr_data_t  MsgId;                  // Message identifier 
    xdr_data_t  MsgLen;                 // absolute length of message
    xdr_data_t  ReplyAddress;           // (player's receiver address
    xdr_data_t  ReplyPort;              // player's receiver port
    char Callsign[MAX_CALLSIGN_LEN];    // Callsign used by the player
};

// Chat message 
struct T_ChatMsg {
    char Text[MAX_CHAT_MSG_LEN];       // Text of chat message
};

// Position message
struct T_PositionMsg {
    char Model[MAX_MODEL_NAME_LEN];    // Name of the aircraft model

    // Time when this packet was generated
    xdr_data2_t time;
    xdr_data2_t lag;

    // position wrt the earth centered frame
    xdr_data2_t position[3];
    // orientation wrt the earth centered frame, stored in the angle axis
    // representation where the angle is coded into the axis length
    xdr_data_t orientation[3];

    // linear velocity wrt the earth centered frame measured in
    // the earth centered frame
    xdr_data_t linearVel[3];
    // angular velocity wrt the earth centered frame measured in
    // the earth centered frame
    xdr_data_t angularVel[3];

    // linear acceleration wrt the earth centered frame measured in
    // the earth centered frame
    xdr_data_t linearAccel[3];
    // angular acceleration wrt the earth centered frame measured in
    // the earth centered frame
    xdr_data_t angularAccel[3];
};

// Property message
struct T_PropertyMsg {
    xdr_data_t id;
    xdr_data_t value;
};

struct FGFloatPropertyData {
  unsigned id;
  float value;
};

// Position message
struct FGExternalMotionData {
  // simulation time when this packet was generated
  double time;
  // the artificial lag the client should stay behind the average
  // simulation time to arrival time diference
  // FIXME: should be some 'per model' instead of 'per packet' property
  double lag;
  
  // position wrt the earth centered frame
  SGVec3d position;
  // orientation wrt the earth centered frame
  SGQuatf orientation;
  
  // linear velocity wrt the earth centered frame measured in
  // the earth centered frame
  SGVec3f linearVel;
  // angular velocity wrt the earth centered frame measured in
  // the earth centered frame
  SGVec3f angularVel;
  
  // linear acceleration wrt the earth centered frame measured in
  // the earth centered frame
  SGVec3f linearAccel;
  // angular acceleration wrt the earth centered frame measured in
  // the earth centered frame
  SGVec3f angularAccel;
  
  // The set of properties recieved for this timeslot
  std::vector<FGFloatPropertyData> properties;
};


// Message buffer
union MsgBuf
{
    MsgBuf()
    {
        memset(&Msg, 0, sizeof(Msg));
    }

    T_MsgHdr* msgHdr()
    {
        return &Header;
    }

    const T_MsgHdr* msgHdr() const
    {
        return reinterpret_cast<const T_MsgHdr*>(&Header);
    }

    T_PositionMsg* posMsg()
    {
        return reinterpret_cast<T_PositionMsg*>(Msg + sizeof(T_MsgHdr));
    }

    const T_PositionMsg* posMsg() const
    {
        return reinterpret_cast<const T_PositionMsg*>(Msg + sizeof(T_MsgHdr));
    }

    xdr_data_t* properties()
    {
        return reinterpret_cast<xdr_data_t*>(Msg + sizeof(T_MsgHdr)
                                             + sizeof(T_PositionMsg));
    }

    const xdr_data_t* properties() const
    {
        return reinterpret_cast<const xdr_data_t*>(Msg + sizeof(T_MsgHdr)
                                                   + sizeof(T_PositionMsg));
    }

    // The end of the properties buffer.
    xdr_data_t* propsEnd()
    {
        return reinterpret_cast<xdr_data_t*>(Msg + MAXI_PACKET_SIZE);
    };

    const xdr_data_t* propsEnd() const
    {
        return reinterpret_cast<const xdr_data_t*>(Msg + MAXI_PACKET_SIZE);
    };

    // The end of properties actually in the buffer. This assumes that
    // the message header is valid.
    xdr_data_t* propsRecvdEnd()
    {
        return reinterpret_cast<xdr_data_t*>(Msg + Header.MsgLen);
    }

    const xdr_data_t* propsRecvdEnd() const
    {
        return reinterpret_cast<const xdr_data_t*>(Msg + Header.MsgLen);
    }

    xdr_data2_t double_val;
    char Msg[MAXI_PACKET_SIZE];
    T_MsgHdr Header;
};

#endif
